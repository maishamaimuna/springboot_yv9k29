CREATE TABLE IF NOT EXISTS categories (
                                          id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                          name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS product_details (
                                               id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                               description TEXT
);


CREATE TABLE IF NOT EXISTS products (
                                        id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                        name VARCHAR(255) NOT NULL,
                                        price DOUBLE NOT NULL,
                                        category_id BIGINT,
                                        product_detail_id BIGINT,
                                        FOREIGN KEY (category_id) REFERENCES categories(id),
                                        FOREIGN KEY (product_detail_id) REFERENCES product_details(id)
);

CREATE TABLE IF NOT EXISTS product_details (
                                               id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                               description TEXT
);

CREATE TABLE IF NOT EXISTS tags (
                                    id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS product_tags (
                                            product_id BIGINT,
                                            tag_id BIGINT,
                                            PRIMARY KEY (product_id, tag_id),
                                            FOREIGN KEY (product_id) REFERENCES products(id),
                                            FOREIGN KEY (tag_id) REFERENCES tags(id)
);
