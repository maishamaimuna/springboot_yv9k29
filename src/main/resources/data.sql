-- Disable foreign key checks
SET REFERENTIAL_INTEGRITY FALSE;

-- Truncate tables to delete all data
TRUNCATE TABLE product_tags;
TRUNCATE TABLE products;
TRUNCATE TABLE categories;
TRUNCATE TABLE product_details;
TRUNCATE TABLE tags;

-- Insert data into the categories table
INSERT INTO categories (name) VALUES
                                  ('Electronics'),
                                  ('Books'),
                                  ('Clothing');

-- Insert data into the product_details table
INSERT INTO product_details (description) VALUES
                                              ('A high-performance laptop with 16GB RAM and 512GB SSD.'),
                                              ('A latest model smartphone with an excellent camera.'),
                                              ('A fascinating book on modern software development practices.');

-- Insert data into the products table
INSERT INTO products (name, price, category_id, product_detail_id) VALUES
                                                                       ('Laptop', 999.99, (SELECT id FROM categories WHERE name = 'Electronics'), 1),
                                                                       ('Smartphone', 699.99, (SELECT id FROM categories WHERE name = 'Electronics'), 2),
                                                                       ('Book', 19.99, (SELECT id FROM categories WHERE name = 'Books'), 3);

-- Insert data into the tags table
INSERT INTO tags (name) VALUES
                            ('New'),
                            ('Sale'),
                            ('Popular');

-- Insert data into the product_tags table (Many-to-Many relationships)
INSERT INTO product_tags (product_id, tag_id) VALUES
                                                  ((SELECT id FROM products WHERE name = 'Laptop'), (SELECT id FROM tags WHERE name = 'New')),
                                                  ((SELECT id FROM products WHERE name = 'Laptop'), (SELECT id FROM tags WHERE name = 'Popular')),
                                                  ((SELECT id FROM products WHERE name = 'Smartphone'), (SELECT id FROM tags WHERE name = 'New')),
                                                  ((SELECT id FROM products WHERE name = 'Smartphone'), (SELECT id FROM tags WHERE name = 'Sale')),
                                                  ((SELECT id FROM products WHERE name = 'Book'), (SELECT id FROM tags WHERE name = 'Popular'));

-- Re-enable foreign key checks
SET REFERENTIAL_INTEGRITY TRUE;

