package com.example.demo.controller;

import com.example.demo.model.Product;
import com.example.demo.model.Category;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.Optional;

@Controller
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;


    @GetMapping("/login")
    public String login() {
        return "login"; // This should match the name of your HTML file (login.html)
    }

    @GetMapping("/welcome")
    public String getWelcomePage() {
        return "welcome"; // This should match the name of your HTML file (welcome.html)
    }

    @GetMapping("/products")
    public String getProductPage(Model model) {
        try {
            model.addAttribute("products", productRepository.findAll());
            model.addAttribute("categories", categoryRepository.findAll()); // Add this line
            return "product";
        } catch (Exception e) {
            model.addAttribute("error", "An error occurred while retrieving products.");
            return "error";
        }
    }


    @PostMapping("/products")
    public String addProduct(@RequestParam String name, @RequestParam double price, @RequestParam Long categoryId) {
        Optional<Category> categoryOpt = categoryRepository.findById(categoryId);
        if (categoryOpt.isPresent()) {
            Product product = new Product();
            product.setName(name);
            product.setPrice(price);
            product.setCategory(categoryOpt.get());
            productRepository.save(product);
            return "redirect:/products";
        } else {
            return "error"; // Handle the case where the category is not found
        }
    }

    @GetMapping("/products/edit/{id}")
    public String editProductForm(@PathVariable("id") Long id, Model model) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            model.addAttribute("product", product.get());
            model.addAttribute("categories", categoryRepository.findAll());
            return "edit_product";
        } else {
            return "redirect:/products";
        }
    }

    @PostMapping("/products/edit/{id}")
    public String updateProduct(@PathVariable("id") Long id, @RequestParam String name, @RequestParam double price, @RequestParam Long categoryId, Model model) {
        if (!categoryRepository.existsById(categoryId)) {
            model.addAttribute("error", "Invalid category ID");
            return "redirect:/products/edit/" + id;
        }
        Optional<Product> productOpt = productRepository.findById(id);
        Optional<Category> categoryOpt = categoryRepository.findById(categoryId);
        if (productOpt.isPresent() && categoryOpt.isPresent()) {
            Product product = productOpt.get();
            product.setName(name);
            product.setPrice(price);
            product.setCategory(categoryOpt.get());
            productRepository.save(product);
            return "redirect:/products";
        } else {
            return "error"; // Handle the case where the product or category is not found
        }
    }


    @GetMapping("/products/delete/{id}")
    public String deleteProduct(@PathVariable Long id) {
        productRepository.deleteById(id);
        return "redirect:/products";
    }
}
