package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorizeRequests -> {
                            authorizeRequests
                                    .requestMatchers("/login", "/h2-console/**").permitAll() // Allow access to login and H2 console without authentication
                                    .requestMatchers("/", "/welcome", "/products").authenticated() // Require authentication for these paths
                                    .anyRequest().authenticated();
                        } // Any other requests must be authenticated
                )
                .formLogin(formLogin ->
                        formLogin
                                .loginPage("/login") // Custom login page
                                .defaultSuccessUrl("/welcome", true) // Redirect to /welcome after successful login
                                .permitAll()
                )
                .logout(logout ->
                        logout
                                .logoutSuccessUrl("/login?logout") // Redirect to login page after logout
                                .permitAll()
                )
                .csrf(csrf -> csrf.disable()) // Disable CSRF for H2 console access (not recommended for production)
                .headers(headers -> headers.frameOptions().disable()); // Disable frame options for H2 console access

        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        UserDetails user = User.withUsername("maisha")
                .password(passwordEncoder().encode("yv9k29"))
                .roles("USER")
                .build();

        return new InMemoryUserDetailsManager(user);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
